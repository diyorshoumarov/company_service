package service

import (
	"company_service/genproto/company_service"
	"company_service/pkg/helper"
	"company_service/pkg/logger"
	"company_service/storage"
	"context"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type companyService struct {
	logger  logger.Logger
	storage storage.StorageI
	company_service.UnimplementedCompanyServiceServer
}

func NewCompanyService(log logger.Logger, db *sqlx.DB) *companyService {
	return &companyService{
		logger:  log,
		storage: storage.NewStoragePG(db),
	}
}

func (s *companyService) Create(ctx context.Context, req *company_service.CreateCompany) (*company_service.CompanyId, error) {
	id, err := s.storage.Company().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while creating company ", req, codes.Internal)
	}

	return &company_service.CompanyId{
		Id: id,
	}, nil
}

func (s *companyService) Get(ctx context.Context, req *company_service.CompanyId) (*company_service.Company, error) {
	company, err := s.storage.Company().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting company ", req, codes.Internal)
	}

	return company, nil
}

func (s *companyService) GetAll(ctx context.Context, req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	companys, err := s.storage.Company().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all company ", req, codes.Internal)
	}

	return companys, nil
}

func (s *companyService) Update(ctx context.Context, req *company_service.Company) (*company_service.MsgResponse, error) {
	msg, err := s.storage.Company().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while updating company ", req, codes.Internal)
	}

	return &company_service.MsgResponse{
		Msg: msg.Msg,
	}, nil
}

func (s *companyService) Delete(ctx context.Context, req *company_service.CompanyId) (*company_service.MsgResponse, error) {
	msg, err := s.storage.Company().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting company ", req, codes.Internal)
	}

	return &company_service.MsgResponse{
		Msg: msg.Msg,
	}, nil
}
