package repo

import "company_service/genproto/company_service"

type CompanyRepoI interface {
	Create(req *company_service.CreateCompany) (string, error)
	Get(id string) (*company_service.Company, error)
	GetAll(req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error)
	Update(req *company_service.Company) (*company_service.MsgResponse, error)
	Delete(id string) (*company_service.MsgResponse, error)
}
