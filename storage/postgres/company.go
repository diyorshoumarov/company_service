package postgres

import (
	"company_service/genproto/company_service"
	"company_service/storage/repo"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type CompanyRepo struct {
	db *sqlx.DB
}

func NewCompanyRepo(db *sqlx.DB) repo.CompanyRepoI {
	return &CompanyRepo{
		db: db,
	}
}

func (r *CompanyRepo) Create(req *company_service.CreateCompany) (string, error) {
	var id uuid.UUID

	tx, err := r.db.Begin()

	if err != nil {
		return "", err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := `
		INSERT INTO 
			company
			(
				id,
				name
			)
			VALUES
			(
				$1,
				$2
			)
	`

	_, err = tx.Exec(query, id, req.Name)
	if err != nil {
		return "", err
	}

	return id.String(), nil
}

func (r *CompanyRepo) Get(id string) (*company_service.Company, error) {
	var company company_service.Company

	query := `
		SELECT id, name
		FROM company
		WHERE id = $1
	`

	row := r.db.QueryRow(query, id)
	err := row.Scan(
		&company.Id,
		&company.Name,
	)

	if err != nil {
		return nil, err
	}

	return &company, nil
}

func (r *CompanyRepo) GetAll(req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	var (
		args      = make(map[string]interface{})
		filter    string
		companies []*company_service.Company
		count     uint32
	)

	if req.Name != "" {
		filter += ` AND name ilike '%' || :name || '%' `
		args["name"] = req.Name
	}

	countQuery := `
		SELECT count(1) 
		FROM company 
		WHERE true 
		` + filter

	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(
			&count,
		)

		if err != nil {
			return nil, err
		}
	}

	filter += " OFFSET :offset LIMIT :limit "
	args["limit"] = req.Limit
	args["offset"] = req.Offset

	query := `
		SELECT id, name
		FROM company 
		WHERE true 
		` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var company company_service.Company

		err = rows.Scan(
			&company.Id,
			&company.Name,
		)

		if err != nil {
			return nil, err
		}

		companies = append(companies, &company)
	}

	return &company_service.GetAllCompanyResponse{
		Companies: companies,
		Count:     count,
	}, nil
}

func (r *CompanyRepo) Update(req *company_service.Company) (*company_service.MsgResponse, error) {
	query := `
		UPDATE company
		SET name = $2
		WHERE id = $1
	`
	_, err := r.db.Query(query, req.Id, req.Name)

	if err != nil {
		return &company_service.MsgResponse{
			Msg: "Not Updated",
		}, err
	}

	return &company_service.MsgResponse{
		Msg: "Updated",
	}, err
}

func (r *CompanyRepo) Delete(id string) (*company_service.MsgResponse, error) {
	_, err := r.db.Exec(
		`DELETE FROM company WHERE id = $1;`,
		id,
	)

	if err != nil {
		return &company_service.MsgResponse{
			Msg: "Not Deleted",
		}, err
	}

	return &company_service.MsgResponse{
		Msg: "Deleted",
	}, err
}
