package storage

import (
	"company_service/storage/postgres"
	"company_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Company() repo.CompanyRepoI
}

type storagePG struct {
	company repo.CompanyRepoI
}

func NewStoragePG(db *sqlx.DB) StorageI {
	return &storagePG{
		company: postgres.NewCompanyRepo(db),
	}
}

func (s *storagePG) Company() repo.CompanyRepoI {
	return s.company
}
